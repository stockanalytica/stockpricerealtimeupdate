package models



// DSEShare ...
type DSEShare struct {
	ID               int     `json:"id"`
	TradingCode      string  `json:"trading_code"`
	LTP              float64 `json:"ltp"`
	High             float64 `json:"high"`
	Low              float64 `json:"low"`
	CloseP           float64 `json:"close_p"`
	YCP              float64 `json:"ycp"`
	Change           float64 `json:"change"`
	ChangePercentage float64 `json:"change_percentage"`
	Trade            int64   `json:"trade"`
	ValueInMN        float64 `json:"value"`
	Volume           int64   `json:"volume"`
}


// Result ...
type Result struct {
	ID                int64
	LastUpdatedOnDate string
	LastUpdatedOnTime string
	Data              []*DSEShare
	MarketStatus      string
}