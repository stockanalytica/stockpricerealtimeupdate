module github.com/diptomondal007/stockpricerealtimeupdate

go 1.15

require (
	github.com/antchfx/xpath v1.1.10 // indirect
	github.com/diptomondal007/bdstockexchange v0.0.0-20200821220157-84eade350610
	github.com/jinzhu/gorm v1.9.16
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/subosito/gotenv v1.2.0
	go.mongodb.org/mongo-driver v1.4.1
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc // indirect
	golang.org/x/text v0.3.3 // indirect
)
