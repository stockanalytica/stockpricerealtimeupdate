package config

import (
	"os"
)

// AppCfg ...
type AppCfg struct {
	Port string
}

var appCfg *AppCfg

// LoadAppCfg ...
func LoadAppCfg() {
	appCfg = &AppCfg{
		Port: os.Getenv("PORT"),
	}
}

// GetAppCfg ...
func GetAppCfg() *AppCfg {
	return appCfg
}
