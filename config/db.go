package config

import (
	"os"
)

// DBCfg ...
type DBCfg struct {
	DBUri  string
	DBName string
}

var dbCfg *DBCfg

// LoadDBCfg ...
func LoadDBCfg() {
	dbCfg = &DBCfg{
		DBUri:  os.Getenv("DB_URI"),
		DBName: os.Getenv("DB_NAME"),
	}
}

// GetDBCfg ...
func GetDBCfg() *DBCfg {
	return dbCfg
}
