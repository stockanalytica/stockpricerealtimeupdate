package conn

import (
	"context"
	"github.com/diptomondal007/stockpricerealtimeupdate/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"time"
)

// MongoDBClient ...
type MongoDBClient struct {
	Client *mongo.Client
}

var mongoClient *MongoDBClient

// ConnectDB ...
func ConnectDB(cfg *config.DBCfg) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(cfg.DBUri))
	if err != nil {
		return err
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return err
	}
	log.Println("got ping! connected to db ...")
	mongoClient = &MongoDBClient{client}
	return nil
}

// GetMongoClient ...
func GetMongoClient() *MongoDBClient {
	return mongoClient
}
