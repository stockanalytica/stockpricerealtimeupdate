package services

import (
	"context"
	"log"
	"time"

	"github.com/diptomondal007/stockpricerealtimeupdate/config"
	"github.com/diptomondal007/stockpricerealtimeupdate/conn"
	"github.com/diptomondal007/stockpricerealtimeupdate/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/diptomondal007/bdstockexchange"
)

// UpdateListener ...
func UpdateListener() {
	for {
		loc, _ := time.LoadLocation("Asia/Dhaka")
		now := time.Now().In(loc)
		if now.Hour() >= 10 && (now.Weekday() != time.Saturday) && (now.Weekday() != time.Friday) && now.Hour() < 17 {
			log.Println(now.Weekday())
			log.Println("listening to update started ...")
			ticker := time.NewTicker(2 * time.Minute)
			func() {
				stop := make(chan bool, 1)
				for {
					nowTime := time.Now().In(loc)
					select {
					case <-ticker.C:
						log.Println(nowTime)
						if nowTime.Hour() > 16 {
							log.Println(">>>day end<<<")
							stop <- true
						}
						err := getLatestData()
						if err != nil {
							log.Println(err)
						}
					case <-stop:
						return
					}
				}
				//close(stop)
			}()

		}

	}
}

func getLatestData() error {
	log.Println("<<< get latest data >>>")
	dse := bdstockexchange.NewDSE()
	var marketStatus string
	arr, err := dse.GetLatestPrices(bdstockexchange.SortByTradingCode, bdstockexchange.ASC)
	if err != nil {
		log.Println(err)
		return err
	}
	shareArr := make([]*models.DSEShare, 0)
	for _, v := range arr {
		dseShare := &models.DSEShare{
			ID:               v.ID,
			TradingCode:      v.TradingCode,
			LTP:              v.LTP,
			High:             v.High,
			Low:              v.Low,
			CloseP:           v.CloseP,
			YCP:              v.YCP,
			Change:           v.Change,
			ChangePercentage: getStockChangePercentage(v.LTP, v.YCP),
			Trade:            v.Trade,
			ValueInMN:        v.ValueInMN,
			Volume:           v.Volume,
		}
		shareArr = append(shareArr, dseShare)
	}

	status, err := dse.GetMarketStatus()
	if err != nil {
		log.Println(err)
		return err
	}

	if status.IsOpen {
		marketStatus = "Open"
	} else {
		marketStatus = "Closed"
	}

	res := &models.Result{
		LastUpdatedOnDate: status.LastUpdatedOn.Date,
		LastUpdatedOnTime: status.LastUpdatedOn.Time,
		Data:              shareArr,
		MarketStatus:      marketStatus,
	}

	err = dbUpdater(res)
	if err != nil {
		return err
	}
	return nil
}

func getStockChangePercentage(LTP, YCP float64) float64 {
	if YCP == 0 {
		return 0
	}
	changePercentage := (LTP - YCP) * 100 / YCP
	return changePercentage
}

func dbUpdater(r *models.Result) error {
	client := conn.GetMongoClient()
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"_id", -1}})
	findOptions.SetLimit(1)
	cur, err := client.Client.Database(config.GetDBCfg().DBName).Collection("data").
		Find(context.Background(), bson.D{}, findOptions)
	if err != nil {
		log.Println(err)
		return err
	}

	for cur.Next(context.TODO()) {
		var res *models.Result
		_ = cur.Decode(&res)
		if r.LastUpdatedOnDate != res.LastUpdatedOnDate || r.LastUpdatedOnTime != res.LastUpdatedOnTime {
			client.Client.Database(config.GetDBCfg().DBName).Collection("data").
				InsertOne(context.Background(), r)
			log.Println("latest stock data inserted ", r.LastUpdatedOnDate, " ", r.LastUpdatedOnTime)
		} else if res.LastUpdatedOnTime == "" {
			client.Client.Database(config.GetDBCfg().DBName).Collection("data").
				InsertOne(context.Background(), r)
			log.Println("latest stock data inserted ", r.LastUpdatedOnDate, " ", r.LastUpdatedOnTime)
		}
	}
	//var res models.Result
	////db.Raw("select distinct on (public.latest_data.id) * from public.latest_data order by public.latest_data.id desc limit 1").Scan(&res)
	////if r.LastUpdatedOnDate != res.LastUpdatedOnDate || r.LastUpdatedOnTime != res.LastUpdatedOnTime {
	////	db.Exec(
	////		"insert into public.latest_data(last_updated_on_date,last_updated_on_time, data, market_status) values(?, ?, ? , ?)",
	////		r.LastUpdatedOnDate, r.LastUpdatedOnTime, r.Data, r.MarketStatus)
	////	log.Println("latest stock data inserted ", r.LastUpdatedOnDate, " ", r.LastUpdatedOnTime)
	////}
	return nil
}
