package cmd

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/diptomondal007/stockpricerealtimeupdate/config"
	"github.com/diptomondal007/stockpricerealtimeupdate/conn"
	"github.com/diptomondal007/stockpricerealtimeupdate/services"
	"github.com/spf13/cobra"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: " Serve Command",
	Long:  "Serve Command",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		if err := conn.ConnectDB(config.GetDBCfg()); err != nil {
			log.Println("DBConnErr:", err.Error())
			return err
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		stop := make(chan os.Signal, 1)
		go services.UpdateListener()
		signal.Notify(stop, syscall.SIGKILL, syscall.SIGINT, syscall.SIGQUIT)
		<-stop
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)
}
