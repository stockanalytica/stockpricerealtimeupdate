package cmd

import (
	"log"

	"github.com/diptomondal007/stockpricerealtimeupdate/config"
	"github.com/spf13/cobra"
	"github.com/subosito/gotenv"
)

// rootCmd represents the base command when called without any sub-commands
var rootCmd = &cobra.Command{
	Use:   "realtime",
	Short: "Stock market realtime update",
	Long:  "Stock market realtime update",
}

// Execute ...
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatalln(err)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

// initConfig reads config from dotenv
func initConfig() {
	gotenv.Load()
	config.LoadDBCfg()
}
